const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')
const cors = require('cors');
const app = express()

const multer = require('multer');
const multerS3 = require('multer-s3');

const S3 = require('aws-sdk/clients/s3');
const uuid = require('uuid/v4');

const upload = multer({
  storage: multerS3({
    s3: new S3(),
    bucket: `downloads.no8.io`,
    storageClass: 'REDUCED_REDUNDANCY',
    key: (req, file, cb) => {
      cb(null, `${uuid()}_${file.originalname}`)
    },
    contentType: (req, file, cb) => {
      cb(null, file.mimetype)
    }
  })
});

const axiosServer = axios.create({ baseURL: process.env.DATASERVER_API_ENDPOINT || 'http://localhost:3000' })
const axiosPartner = axios.create({ baseURL: process.env.PARTNER_API_ENDPOINT || 'https://partner-api-d.no8.io' })
const API_SERVER_ENDPOINT = process.env.API_SERVER_ENDPOINT || 'http://localhost:3001'
const AUTH = process.env.AUTH
const PORT = process.env.PORT || 3001

const defaultCustomer = {
  id: 'default',
  cart: [],
  totalAmount: 0,
  date: '',
  numbers: 0,
  roomName: '',
  roomPrice: 0
}

app.set('view engine', 'ejs')
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/public', express.static(path.join(__dirname + '/public')))

app.post('/upload', upload.array('fileVal', 5), (req, res) => {
  const location = req.files[0].location.split('downloads.no8.io')[1]
  res.status(200).json({ url: `https://downloads.no8.io${location}` });
});

app.get('/api/:sender/cart', (req, res) => {
  const { sender } = req.params
  axiosServer.get(`/customer/?id=${sender}`)
    .then(response => res.status(200).json(response.data[0].cart))
})

app.get('/api/:sender/customer', (req, res) => {
  const { sender } = req.params
  axiosServer.get(`/customer/?id=${sender}`)
    .then(response => res.status(200).json(response.data[0]))
})

app.get('/api/products', (req, res) => {
  axiosServer.get('/products')
    .then(response => res.status(200).json(response.data))
})

app.post('/api/:sender/cart', (req, res) => {
  const { sender } = req.params
  const { id } = req.body

  if (id) {
    axiosServer.get(`/customer?id=${sender}`)
      .then(response => {
        const customer = response.data[0]
        if (customer) {
          const product = customer.cart.find(prod => prod.id === +id)
          if (product) {
            product.quantity = product.quantity + (req.body.quantity || 1)
            customer.cart = customer.cart.map(item => item.id === +id ? product : item)
            customer.totalAmount = caculateTotalAmount(customer.cart)
            return axiosServer.put(`/customer/${sender}`, customer)
              .then(customerResponse => {
                res.status(200).json({
                  title: product.title,
                  totalAmount: customer.totalAmount
                })
              })
          } else {
            return axiosServer.get(`/products?id=${id}`)
              .then(productsResponse => {
                const product = productsResponse.data[0]
                if (product) {
                  product.quantity = req.body.quantity || 1
                  customer.cart.push(product)
                  customer.totalAmount = caculateTotalAmount(customer.cart)
                  return axiosServer.put(`/customer/${sender}`, customer)
                    .then(() => {
                      res.status(200).json({
                        title: product.title,
                        totalAmount: customer.totalAmount
                      })
                    })
                }
              })
          }
        } else {
          return axiosServer.get(`/products?id=${id}`)
            .then(response => {
              const product = response.data[0]
              if (product) {
                product.quantity = req.body.quantity || 1
                return axiosServer.post('/customer', Object.assign({}, defaultCustomer, {
                  id: sender,
                  cart: [product],
                  totalAmount: product.price
                }))
                  .then(() => {
                    res.status(200).json({
                      title: product.title,
                      totalAmount: product.price
                    })
                  })
              } else {
                res.sendStatus(400)
              }
            })
        }
      })
  } else {
    res.sendStatus(400)
  }
})

app.delete('/api/:sender/cart', (req, res) => {
  const { sender } = req.params
  const { id } = req.body

  axiosServer.get(`/customer?id=${sender}`)
    .then(response => {
      const customer = response.data[0]
      let deleteProduct = {}
      customer.cart = customer.cart.filter(product => {
        if (product.id === +id) {
          deleteProduct = product
        }
        return product.id !== +id
      })
      customer.totalAmount = caculateTotalAmount(customer.cart)
      deleteProduct.totalAmount = customer.totalAmount
      axiosServer.put(`/customer/${sender}`, customer)
        .then(() => res.status(200).json(deleteProduct))
    })
})

app.post('/api/:sender/checkout', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params

  axiosPartner(buildTextMesssage(sender, '您已經付款成功！將於在 3-5 天內出貨，謝謝您的購買！😊 希望您繼續支持無毒無籽檸檬唷 🍋'))
    .then(() => axiosServer.get(`/customer?id=${sender}`))
    .then(response => {
      const customer = response.data[0]
      if (customer) {
        customer.cart = []
        customer.totalAmount = 0
        return axiosServer.put(`/customer/${sender}`, customer)
      }
      return null
    })
})

app.post('/api/:sender/payment-villa', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params

  axiosPartner(buildTextMesssage(sender, '經由我們核對無誤後，會以訊息通知您，再麻煩留意一下，謝謝您的預定！假期愉快！🚙 👜 '))
})

app.post('/api/:sender/datepicker/restaurant', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params
  const { date } = req.body

  axiosPartner(buildTextMesssage(sender, `您選擇的時間為📅${date}`))
    .then(() => {
      const content = {
        elements: [
          {
            title: '請問有幾位要用餐？',
            buttons: [
              {
                type: 'url',
                title: '選擇人數',
                data: `${API_SERVER_ENDPOINT}/webview/${sender}/itempicker`,
                facebook: {
                  webview_height_ratio: 'compact',
                  messenger_extensions: true
                }
              }
            ]
          }
        ],
        templateType: 'card'
      }
      return axiosPartner(buildTemplageMessage(sender, content))
    })
    .then(() => axiosServer.get(`/customer?id=${sender}`))
    .then(response => {
      const customer = response.data[0]
      if (customer) {
        customer.date = date
        axiosServer.put(`/customer/${sender}`, customer)
      } else {
        axiosServer.post('/customer', Object.assign({}, defaultCustomer, {
          id: sender,
          date
        }))
      }
    })
})

app.post('/api/:sender/datepicker/service', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params
  const { date, projectId } = req.body

  axiosPartner(buildTextMesssage(sender, `您選擇的時間為📅${date}`))
    .then(() => axiosPartner(buildTextMesssage(sender, '以下是有時間為您服務的美甲師，您想要預約哪一位美甲師呢？🙎 🙆 💁')))
    .then(() => {
      const content = {
        elements: [
          {
            title: 'Tammy 店長',
            subtitle: '這個時間我有空唷！',
            buttons: [
              {
                type: 'postback',
                title: '選擇',
                data: 'Tammy 店長'
              }
            ],
            imageUrl: 'https://assets.no8.io/%E7%BE%8E%E7%94%B2%E5%B8%AB-Tammy.png'
          },
          {
            title: '貝蒂',
            subtitle: '這個時間我有空唷！',
            buttons: [
              {
                type: 'postback',
                title: '選擇',
                data: '貝蒂'
              }
            ],
            imageUrl: 'https://assets.no8.io/%E7%BE%8E%E7%94%B2%E5%B8%AB-%E8%B2%9D%E8%92%82.png'
          }
        ],
        templateType: 'card'
      }
      return axiosPartner(buildTemplageMessage(sender, content))
    })
    .then(() => axiosServer.get(`/customer?id=${sender}`))
    .then(response => {
      getServiceProjectName(projectId)
        .then(serviceName => {
          const customer = response.data[0]
          if (customer) {
            customer.date = date
            customer.serviceName = serviceName
            axiosServer.put(`/customer/${sender}`, customer)
          } else {
            axiosServer.post('/customer', Object.assign({}, defaultCustomer, {
              id: sender,
              date,
              serviceName
            }))
          }
        })
    })
})

app.post('/api/:sender/datepicker/accommodation', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params
  const date = req.body.date.split(' ')[0]

  axiosPartner(buildTextMesssage(sender, `您選擇的時間為📅${date}`))
    .then(() => axiosPartner(buildTextMesssage(sender, '請問要預定哪一個房型？')))
    .then(() => {
      const content = {
        elements: [
          {
            title: '🛌 舒適簡潔的房型',
            subtitle: '2人房\n💰 NT. 2400',
            buttons: [
              {
                type: 'postback',
                title: '選擇簡潔2人',
                data: "1"
              }
            ],
            imageUrl: 'https://assets.no8.io/room01.png'
          },
          {
            title: '🛌🛌 舒適簡潔的四人房',
            subtitle: '4人房\n💰 NT. 3200',
            buttons: [
              {
                type: 'postback',
                title: '選擇簡潔4人',
                data: "2"
              }
            ],
            imageUrl: 'https://assets.no8.io/room02.png'
          },
          {
            title: '🛌🛌 熱帶島嶼的四人房',
            subtitle: '4人房\n💰 NT. 3200',
            buttons: [
              {
                type: 'postback',
                title: '選擇熱帶4人',
                data: "3"
              }
            ],
            imageUrl: 'https://assets.no8.io/room03.png'
          },
          {
            title: '🚪 🛌 整棟是我的全世界',
            subtitle: '2人房 + 4人房兩間\n💰 NT. 9000',
            buttons: [
              {
                type: 'postback',
                title: '選擇包棟',
                data: "4"
              }
            ],
            imageUrl: 'https://assets.no8.io/room04.png'
          }
        ],
        templateType: 'card'
      }
      return axiosPartner(buildTemplageMessage(sender, content))
    })
    .then(() => axiosServer.get(`/customer?id=${sender}`))
    .then(response => {
      const customer = response.data[0]
      if (customer) {
        customer.date = date
        axiosServer.put(`/customer/${sender}`, customer)
      } else {
        axiosServer.post('/customer', Object.assign({}, defaultCustomer, {
          id: sender,
          date
        }))
      }
    })
})

app.post('/api/:sender/room', (req, res) => {
  const { sender } = req.params
  const { id } = req.body
  axiosServer.get(`/customer?id=${sender}`)
    .then(response => {
      const customer = response.data[0]
      if (customer) {
        axiosServer.get(`/rooms?id=${id}`)
          .then(rooms => {
            const room = rooms.data[0]
            if (room) {
              customer.roomName = room.roomName
              customer.roomPrice = room.price * 0.3
              axiosServer.put(`/customer/${sender}`, customer)
                .then(() => {
                  res.status(200).json({
                    roomName: room.roomName,
                    roomPrice: room.price * 0.3
                   })
                })
            }
          })
      } else {
        axiosServer.get(`/rooms?id=${id}`)
          .then(rooms => {
            const room = rooms.data[0]
            if (room) {
              customer.roomName = room.roomName
              customer.roomPrice = room.price * 0.3
              axiosServer.post('/customer',  Object.assign({}, defaultCustomer, {
                id: sender , numbers
               }))
                .then(() => {
                  res.status(200).json({
                    roomName: room.roomName,
                    roomPrice: room.price * 0.3
                  })
                })
            }
          })
      }
    })
})

app.post('/api/:sender/itempicker', (req, res) => {
  res.status(200).json({ ok: true })

  const { sender } = req.params
  const { numbers } = req.body

  axiosPartner(buildTextMesssage(sender, `您選擇的人數為 ${numbers} 位`))
    .then(() => {
      const content = {
        elements: [
          {
            title: '您選擇的時段有空位，確認預定嗎？',
            subtitle: ' ',
            buttons: [
              {
                type: 'postback',
                title: '確認',
                data: '確認'
              },
              {
                type: 'postback',
                title: '取消重訂',
                data: "取消重訂"
              }
            ]
          }
        ],
        templateType: 'card'
      }
      return axiosPartner(buildTemplageMessage(sender, content))
    })
    .then(() => axiosServer.get(`/customer?id=${sender}`))
    .then(response => {
      const customer = response.data[0]
      if (customer) {
        customer.numbers = numbers
        axiosServer.put(`/customer/${sender}`, customer)
      } else {
        axiosServer.post('/customer', Object.assign({}, defaultCustomer, {
          id: sender , numbers
         }))
      }
    })
})

app.get('/api/:sender/services/:id', (req, res) => {
  const { id } = req.params
  axiosServer.get(`/services/${id}`)
    .then(response => {
      res.status(200).json(response.data.projects)
    })
})

app.get('/webview/poc', (req, res) => {
  const proj = req.query.proj, view = req.query.view, user8 = req.query.user8
  res.render(`${proj}/${view}`, { user8: req.query.user8 });
})

app.get('/webview/:sender/payment', (req, res) => {
  res.render('payment', { sender: req.params.sender })
})

app.get('/webview/payment-line', (req, res) => {
  res.render('payment-line', { sender: req.query.sender })
})

app.get('/webview/:sender/product/1', (req, res) => {
  res.render('product-s', { sender: req.params.sender })
})

app.get('/webview/:sender/product/2', (req, res) => {
  res.render('product-l', { sender: req.params.sender })
})

app.get('/webview/:sender/datepicker/restaurant', (req, res) => {
  const { sender, projectId } = req.params
  res.render('datetimepicker', { sender, projectId, type: 'restaurant' })
})

app.get('/webview/:sender/datepicker/service/:projectId', (req, res) => {
  const { sender, projectId } = req.params
  res.render('datetimepicker', { sender, type: 'service', projectId })
})

app.get('/webview/:sender/datepicker/accommodation', (req, res) => {
  const { sender, projectId } = req.params
  res.render('datepicker', { sender, projectId, type: 'accommodation' })
})

app.get('/webview/:sender/itempicker', (req, res) => {
  const { sender } = req.params
  res.render('itempicker', { sender })
})

app.get('/webview/picker', (req, res) => {
  res.render('picker')
})

app.get('/webview/:sender/payment-villa', (req, res) => {
  res.render('payment-villa', { sender: req.params.sender })
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})

function buildOptions(sender) {
  const options = {
    method: 'post',
    url: '/function/broadcast',
    headers: {
      Authorization: AUTH
    },
    data: {
      where: {
        ids: [ sender ]
      }
    }
  }
  return options
}

function buildTextMesssage(sender, content) {
  const options = buildOptions(sender)
  options.data.message = {
    contentType: 'text/plain',
    data: {
      content
    }
  }
  return options
}

function buildTemplageMessage(sender, content) {
  const options = buildOptions(sender)
  if (typeof content === 'string') {
    options.data.template = {
      className: 'Template',
      objectId: content
    }
  } else {
    options.data.template = {
      className: 'Template',
      templateType: content.templateType,
      data: content
    }
  }
  return options
}

function getServiceProjectName(projectId) {
  const id = projectId.split('-')[0]
  return axiosServer.get(`/services/${id}`)
    .then(response => {
      const service = response.data
      const project = service.projects.find(p => p.projectId === projectId)
      return `${service.serviceName}${project.projectName}`
    })
}

function caculateTotalAmount(array) {
  return array.reduce((acc, cur) => acc + (cur.price * cur.quantity), 0)
}
